FROM node:10.2.1 as builder
WORKDIR /usr/src/app
ARG API_URL
ENV REACT_APP_API_URL=$API_URL
COPY package.json yarn.lock ./
RUN yarn
COPY . ./
RUN yarn build

FROM nginx:1.12-alpine
COPY --from=builder /usr/src/app/build /usr/share/nginx/html
COPY ./docker/etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]