if [ "$1" = "production" ]
then
    ssh -tt $USER@$HOST "
        docker login $DOCKER_TSH_REGISTRY -u $DOCKER_TSH_USERNAME -p $DOCKER_TSH_PASSWORD;
        docker stop ryu7_production_ngnix_1;
        docker run -d --rm -p 7003:80 --name ryu7_production_ngnix_1 $DOCKER_TSH_REGISTRY/ryu7-pipelines-ninja:$BITBUCKET_COMMIT
    "
elif [ "$1" = "staging" ]
then
    ssh -tt $USER@$HOST "
        docker login $DOCKER_TSH_REGISTRY -u $DOCKER_TSH_USERNAME -p $DOCKER_TSH_PASSWORD;
        docker stop ryu7_staging_ngnix_1;
        docker run -d --rm -p 7002:80 --name ryu7_taging_ngnix_1 $DOCKER_TSH_REGISTRY/ryu7-pipelines-ninja:$BITBUCKET_COMMIT
    "
else
    ssh -tt $USER@$HOST "
        docker login $DOCKER_TSH_REGISTRY -u $DOCKER_TSH_USERNAME -p $DOCKER_TSH_PASSWORD;
        docker stop ryu7_test_ngnix_1;
        docker run -d --rm -p 7001:80 --name ryu7_test_ngnix_1 $DOCKER_TSH_REGISTRY/ryu7-pipelines-ninja:$BITBUCKET_COMMIT
    "
fi