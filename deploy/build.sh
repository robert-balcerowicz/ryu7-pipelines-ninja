docker login $DOCKER_TSH_REGISTRY -u $DOCKER_TSH_USERNAME -p $DOCKER_TSH_PASSWORD

docker build . -t $DOCKER_TSH_REGISTRY/ryu7-pipelines-ninja:$BITBUCKET_COMMIT \
    --build-arg API_URL="$API_URL"

docker push $DOCKER_TSH_REGISTRY/ryu7-pipelines-ninja:$BITBUCKET_COMMIT