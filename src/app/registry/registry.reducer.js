import { handleActions } from 'redux-actions';

import { FETCH_MEMES_REQUEST, FETCH_MEMES_SUCCESS, FETCH_MEMES_FAILURE } from './registry.action-types';

const defaultState = {
  loading: true,
};

export const registryReducer = handleActions(
  {
    [FETCH_MEMES_REQUEST]: state => ({
      ...state,
      loading: true,
    }),
    [FETCH_MEMES_SUCCESS]: (state, { payload: { data: memes } }) => ({
      ...state,
      memes,
      loading: false,
    }),
    [FETCH_MEMES_FAILURE]: state => ({
      ...state,
      loading: false,
    }),
  },
  defaultState,
);
