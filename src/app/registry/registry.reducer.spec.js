import { registryReducer } from './registry.reducer';

import * as types from './registry.action-types';

describe('Registry reducer', () => {
  it('returns default state', () => {
    expect(registryReducer(undefined, {})).toMatchSnapshot();
  });

  it('handles fetching memes request', () => {
    expect(registryReducer({ foo: 'bar' }, { type: types.FETCH_MEMES_REQUEST })).toMatchSnapshot();
  });

  it('handles fetching memes success', () => {
    expect(
      registryReducer({ foo: 'bar' }, { type: types.FETCH_MEMES_SUCCESS, payload: { data: ['foo', 'bar'] } }),
    ).toMatchSnapshot();
  });

  it('handles fetching memes failure', () => {
    expect(registryReducer({ foo: 'bar' }, { type: types.FETCH_MEMES_REQUEST })).toMatchSnapshot();
  });
});
