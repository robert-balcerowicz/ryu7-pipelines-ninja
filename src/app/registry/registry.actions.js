import { RSAA } from 'redux-api-middleware';

import * as types from './registry.action-types';

export const fetchMemes = () => ({
  [RSAA]: {
    types: [types.FETCH_MEMES_REQUEST, types.FETCH_MEMES_SUCCESS, types.FETCH_MEMES_FAILURE],
    endpoint: `${process.env.REACT_APP_API_URL}/memes`,
    method: 'GET',
  },
});
