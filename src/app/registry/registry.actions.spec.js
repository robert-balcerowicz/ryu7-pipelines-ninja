import * as actions from './registry.actions';

describe('Registry actions', () => {
  it('creates fetch memes action', () => {
    expect(actions.fetchMemes()).toMatchSnapshot();
  });
});
