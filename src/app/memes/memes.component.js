import React, { Component } from 'react';
import { matchPath } from 'react-router';
import * as Vibrant from 'node-vibrant';

import get from 'lodash/get';

import { ItemComponent, LinkComponent } from 'app/shared';
import images from 'assets/library';

import styles from './memes.module.scss';

export class MemesComponent extends Component {
  images = [];

  constructor(props) {
    super(props);

    this.state = {
      colors: {},
    };
  }

  isMemeActive = slug =>
    get(
      matchPath(this.props.location.pathname, {
        path: '/memes/:slug',
      }),
      ['params', 'slug'],
    ) === slug;

  onPrev = index => this.props.history.push(index > 0 ? `/memes/${this.props.memes[index - 1].slug}` : '/');
  onNext = index =>
    index < this.props.memes.length - 1 ? this.props.history.push(`/memes/${this.props.memes[index + 1].slug}`) : null;

  onLoad = index =>
    Vibrant.from(this.images[index])
      .getPalette()
      .then(palette => {
        this.setState(state => ({
          ...state,
          colors: {
            ...state.colors,
            [index]: palette.Vibrant ? palette.Vibrant.getHex() : '#fff',
          },
        }));
      });

  render() {
    return this.props.memes.map((meme, index) => (
      <ItemComponent
        key={index}
        style={{ background: this.state.colors[index] }}
        active={this.isMemeActive(meme.slug)}
        onPrev={() => this.onPrev(index)}
        onNext={() => this.onNext(index)}
      >
        <LinkComponent to={`/memes/${meme.slug}`} />
        <div className={styles.container}>
          <div className={styles.image}>
            <img
              src={images[meme.image]}
              alt={meme.slug}
              ref={c => (this.images[index] = c)}
              onLoad={() => this.onLoad(index)}
            />
          </div>
        </div>
      </ItemComponent>
    ));
  }
}
