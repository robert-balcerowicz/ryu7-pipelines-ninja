import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { MemesComponent } from './memes.component';

const mapStateToProps = ({ registry: { memes }}) => ({
  memes,
});

export const MemesContainer = compose(withRouter, connect(mapStateToProps))(MemesComponent);
