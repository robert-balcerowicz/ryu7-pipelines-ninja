import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import { MemesComponent } from './memes.component';

describe('Memes component', () => {
  it('renders correctly', () => {
    const props = {
      location: {
        pathname: '/memes/foo',
      },
      memes: [
        {
          color: 'red',
          image: 'foo',
          slug: 'foo'
        },
        {
          color: 'green',
          image: 'bar',
          slug: 'bar'
        },
        {
          color: 'blue',
          image: 'baz',
          slug: 'baz'
        }
      ]
    };

    expect(
      renderer
        .create(
          <MemoryRouter initialEntires={['/foo']}>
            <MemesComponent {...props}>foo</MemesComponent>
          </MemoryRouter>,
        )
        .toJSON(),
    ).toMatchSnapshot();
  });
});
