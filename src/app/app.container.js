import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { AppComponent } from './app.component';

import { fetchMemes } from 'app/registry/registry.actions';

const mapStateToProps = ({ registry: { loading, memes } }) => ({
  loading,
  memes,
});

const mapDispatchToProps = dispatch => ({
  fetchMemes: () => dispatch(fetchMemes()),
});

export const AppContainer = compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(AppComponent);
