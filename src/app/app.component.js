import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import { HomeContainer } from './home/home.container';
import { MemesContainer } from './memes/memes.container';

import styles from './app.module.scss';

export class AppComponent extends Component {
  componentDidMount() {
    this.props.fetchMemes();
  }

  render() {
    if (this.props.loading) {
      return null;
    }

    return (
      <div className={styles.wrapper}>
        <NavLink to="/" exact activeClassName={styles.home} />
        {this.props.memes.map((meme, index) => (
          <NavLink to={`/memes/${meme.slug}`} key={index} activeClassName={styles[`meme-${index + 1}`]} />
        ))}

        <div className={styles.grid}>
          <HomeContainer />
          <MemesContainer />
        </div>
      </div>
    );
  }
}
