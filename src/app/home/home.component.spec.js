import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import { HomeComponent } from './home.component';

describe('Home component', () => {
  it('renders correctly', () => {
    const props = {
      location: {
        pathname: '/foo',
      },
    };

    expect(
      renderer
        .create(
          <MemoryRouter initialEntires={['/foo']}>
            <HomeComponent {...props}>foo</HomeComponent>
          </MemoryRouter>,
        )
        .toJSON(),
    ).toMatchSnapshot();
  });

  it('renders active homepage correctly', () => {
    const props = {
      location: {
        pathname: '/',
      },
    };

    expect(
      renderer
        .create(
          <MemoryRouter initialEntries={['/']}>
            <HomeComponent {...props}>foo</HomeComponent>
          </MemoryRouter>,
        )
        .toJSON(),
    ).toMatchSnapshot();
  });
});
