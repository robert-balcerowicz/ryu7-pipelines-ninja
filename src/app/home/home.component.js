import React, { Component } from 'react';
import { matchPath } from 'react-router';

import { ItemComponent, LinkComponent } from 'app/shared';

import styles from './home.module.scss';

export class HomeComponent extends Component {
  isHomeActive = () =>
    !!matchPath(this.props.location.pathname, {
      path: '/',
      exact: true,
    });

  onNext = () => this.props.history.push(`/memes/${this.props.next}`);

  render() {
    return (
      <ItemComponent className={styles.home} active={this.isHomeActive()} onNext={this.onNext}>
        <LinkComponent to="/" />
      </ItemComponent>
    );
  }
}
