import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { HomeComponent } from './home.component';

const mapStateToProps = ({ registry: { memes }}) => ({
  next: memes[0].slug,
});

export const HomeContainer = compose(withRouter, connect(mapStateToProps))(HomeComponent);
