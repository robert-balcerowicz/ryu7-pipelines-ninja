import React, { Component } from 'react';
import classNames from 'classnames';

import styles from './item.module.scss';

export class ItemComponent extends Component {
  componentDidMount() {
    document.addEventListener('keyup', this.onKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keyup', this.onKeyDown);
  }

  onKeyDown = e => {
    if (this.props.active) {
      if (!!this.props.onPrev && e.keyCode === 38) {
        this.props.onPrev();
      } else if (!!this.props.onNext && e.keyCode === 40) {
        this.props.onNext();
      }
    }
  };

  render() {
    const itemClassNames = classNames(styles.item, { [styles.active]: this.props.active }, this.props.className);

    return (
      <div className={itemClassNames} style={this.props.style}>
        {this.props.children}
      </div>
    );
  }
}
