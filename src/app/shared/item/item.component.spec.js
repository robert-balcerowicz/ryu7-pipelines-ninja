import React from 'react';
import renderer from 'react-test-renderer';

import { ItemComponent } from './item.component';

describe('Item component', () => {
  it('renders correctly', () => {
    expect(
      renderer
        .create(
          <ItemComponent className="foo" style={{ background: '#fff' }} active>
            foo
          </ItemComponent>,
        )
        .toJSON(),
    ).toMatchSnapshot();
  });
});
