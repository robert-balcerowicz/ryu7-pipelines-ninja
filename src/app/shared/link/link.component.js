import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

import styles from './link.module.scss';

export class LinkComponent extends Component {
  render() {
    return (
      <NavLink
        activeClassName={styles.active}
        className={classNames(styles.link, this.props.className)}
        to={this.props.to}
        exact
      >
        {this.props.children}
      </NavLink>
    );
  }
}
