import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';

import { LinkComponent } from './link.component';

describe('Link component', () => {
  it('renders correctly', () => {
    expect(
      renderer
        .create(
          <MemoryRouter>
            <LinkComponent className="foo" to="/foo">
              foo
            </LinkComponent>
          </MemoryRouter>,
        )
        .toJSON(),
    ).toMatchSnapshot();
  });
});
