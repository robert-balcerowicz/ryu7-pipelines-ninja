import { combineReducers } from 'redux';

import { registryReducer as registry } from 'app/registry/registry.reducer';

export const appReducer = combineReducers({
  registry,
});
