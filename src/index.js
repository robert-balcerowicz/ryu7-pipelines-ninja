import React from 'react';
import ReactDOM from 'react-dom';

import { applyMiddleware, compose, createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension/logOnlyInProduction';
import { Provider } from 'react-redux';

import { BrowserRouter } from 'react-router-dom';

import { apiMiddleware } from 'redux-api-middleware';

import { AppContainer } from 'app/app.container';
import { appReducer } from 'app/app.reducer';

import registerServiceWorker from './registerServiceWorker';
import './assets/styles/styles.css';

const store = createStore(appReducer, compose(applyMiddleware(apiMiddleware), devToolsEnhancer({})));

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <AppContainer />
      </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
  );
};

render();

if (module.hot) {
  module.hot.accept('app/app.container', () => {
    render();
  });
}

registerServiceWorker();
